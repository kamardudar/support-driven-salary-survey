# Support Driven Salary Survey
## About You
<section class="about-you">
<ol>
    <li>Which option best describes your role at your company?</li>
        <ol type="a">
            <li>I do frontend support only</li>
            <li>I manage a team and so some frontline support</li>
            <li>I only manage a support team</li>
            <li>I am in a director or leadership role on a support team</li>
        </ol>
    </li>
    <br>
    <li>How long have you been in your current role?</li>
        <ol type="a">
            <li>Fewer than 6 months</li>
            <li>7 months to 1 year</li>
            <li>1 to 3 years</li>
            <li>3 to 4 years</li>
            <li>5 to 9 years</li>
            <li>10+ years</li>
        </ol>
    </li>
    <br>
    <li>Do you work full-time or part time in your position?</li>
        <ol type="a">
            <li>Full-time: 30 hours or more</li>
            <li>Part-time: fewer than 30 hours</li>
            <li>Other: please specify</li>
        </ol>
    </li>
    <br>
    <li>Do you work remotely? (i.e., not just an occasional Work From Home day)</li>
        <ol type="a">
            <li>100% of the time</li>
            <li>50% of the time</li>
            <li>Occasionally (regularly but less than 50% of the time)</li>
            <li>No</li>
        </ol>
    </li>
    <br>
    <li>How many years of experience do you have working in customer support?</li>
        <ol type="a">
            <li>Fewer than 6 months</li>
            <li>7 months to 1 year</li>
            <li>1 to 3 years</li>
            <li>3 to 4 years</li>
            <li>5 to 9 years</li>
            <li>10+ years</li>
        </ol>
    </li>
    <br>
    <li>What's your annual salary (in US dollars)?</li>
    </li>
    <br>
    <li>If you had the same role in 2019, what was your salary then (in US dollars)?</li>
    </li>
    <br>
    <li>Did you ask for a raise in 2019-2020?</li>
        <ol type="a">
            <li>Yes</li>
            <li>No</li>
        </ol>
    </li>
    <br>
    <li>As of 2019. have you...</li>
        <ol type="a">
            <li>Changed roles within your company?</li>
            <li>Been promoted within your company?</li>
            <li>Changed companies?</li>
            <li>None of the above</li>
        </ol>
    </li>
    <br>
    <li>What gender do you identify as? <br
    If you prefer to self-describe, please select "Other" and write in your answer</li>
        <ol type="a">
            <li>Male</li>
            <li>Female</li>
            <li>Non-binary/third gender</li>
            <li>I prefer not to say</li>
            <li>Other</li>
        </ol>
    </li>
    <br>
    <li>What ethnicity do you identify as? <br
    Choose as many as you like</li>
        <ol type="a">
            <li>Asian</li>
            <li>Black/African</li>
            <li>Caucasian</li>
            <li>Hispanic/Latinx</li>
            <li>Native American</li>
            <li>Pacific Islander</li>
            <li>I prefer not to say</li>
            <li>Other</li>
        </ol>
    </li>
    <br>
    <li>What's the cost of living where you live?</li>
        <ol type="a">
            <li>$$$$ (e.g., San Francisco, Hong King, Sydney, London, Paris, New York)</li>
            <li>$$$ (e.g., Nashville, Birmingham (US), Vienna, Austin, Las Vegas, Tel Aviv)</li>
            <li>$$ (e.g., Tallinn, San Antonio, Santiago)</li>
            <li>$ (e.g., Manila, Delhi, Hanoi)</li>
        </ol>
    </li>
</ol>
</section>

## About Your Company
<section class="about-your-company">
<ol>
    <li>Which region is your company based in?</li>
        <ol type="a">
            <li>Europe</li>
            <li>North America</li>
            <li>Asia</li>
            <li>South America</li>
            <li>Middle East</li>
            <li>Africa</li>
            <li>Australia/Oceania</li>
            <li>Other</li>
        </ol>
    </li>
    <br>
    <li>What is the approximate total number of employees at your company?</li>
        <ol type="a">
            <li>Fewer than 50</li>
            <li>51 to 100</li>
            <li>101 to 250</li>
            <li>251 to 500</li>
            <li>501 to 1,000</li>
            <li>1,001 to 5,000</li>
            <li>5,001 to 10,000</li>
            <li>More than 10,000 employees</li>
        </ol>
    </li>
    <br>
    <li>What is your business model? <br>
    Choose as many as you like</li>
        <ol type="a">
            <li>We sell to other businesses (B2B)</li>
            <li>We sell to consumers (B2C)</li>
            <li>We sell to enterprise or government (B2E)</li>
            <li>We are a non-profit/government organization</li>
            <li>We have subscription based revenue</li>
            <li>We have sales based revenue</li>
            <li>Other</li>
        </ol>
    </li>
    <br>
    <li>How complex is your product or products?</li>
        <ol type="a">
            <li>Simple (single page application, one product or one feature set, one platform, etc)</li>
            <li>Moderate (one product but multiple platforms, or multiple products but only one version and/or platform for each)</li>
            <li>We sell to enterprise or government (B2E)</li>
            <li>Complex (multiple products and features, multiple platforms, etc)</li>
        </ol>
    </li>
    <br>
    <li>What is the size of your customer support team?</li>
        <ol type="a">
            <li>Fewer than 5</li>
            <li>5 - 20</li>
            <li>21 - 50</li>
            <li>51 - 100</li>
            <li>101 - 500</li>
            <li>501 or more</li>
        </ol>
    </li>
    <br>
    <li>Do you receive other forms of compensaion?<br>
    Choose as many as you like</li>
        <ol type="a">
            <li>Medical benefits (partially paid -> 100%)</li>
            <li>Bonuses</li>
            <li>Amazing benefits (Company transportation, free food, sabbaticals, etc.)</li>
            <li>Equity</li>
            <li>Work from home policy</li>
            <li>Professional development budget (conferences, continuing education, seminars, books)</li>
            <li>Maternity time off and/or paternity time off more than FMLA</li>
            <li>401K or other retirement plan options</li>
            <li>I don't recieve any other forms of compensation</li>
            <li>Other</li>
        </ol>
    </li>
    <br>
</ol>  
</section>

## Bonus Questions
<section class="bonus-quesitons">
<ol>
    <li>What skill do you want to learn or develop for yourself?</li>
    </li>
    <br>
    <li>If you are a manager what would you want your direct report(s) to learn or develop as a skill?</li>
    </li>
    <br>
</ol>
</section>  
